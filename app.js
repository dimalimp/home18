// 1-------------------------------------------------
// функция конструктор

function CounterOne(count) {
    this.count = count;
    this.sum = function (num) {
        this.count = this.count + num
        return this.count;
    };
    this.sub = function (num) {
        this.count = this.count - num
        return this.count
    }
}

const numberOne = new CounterOne(0)
console.log(numberOne.sum(2))
console.log(numberOne.sum(5))
console.log(numberOne.sub(3))

// 2-------------------------------------------------
// классы

class Counter {
    #count
    constructor() {
        this.#count = 0
    }
    sum = (num) => {
        this.#count = this.#count + num
    };
    sub = (num) => {
        this.#count = this.#count - num
    }
    get getSum() {
        return this.#count;
    }
    set getSum(num) {
        this.#count = num;
    }
}

const number = new Counter()
number.sum(3)
number.sub(1)
console.log(number.getSum)
number.getSum = 10
number.sum(2)
console.log(number.getSum)

// 3-----------------------------------

class Human {
    constructor(age, eat) {
        this.age = age;
        this.oneEat = eat;
        this.maxEat = 100;
        this.newEat = 0;
    };
    eat = () => {
        if (this.newEat + this.oneEat <= this.maxEat) {
            return this.newEat = this.newEat + this.oneEat
        } else {
            this.newEat = this.maxEat;
            console.log("человек сыт");
        }
    }
}

class Woman extends Human {
    constructor(age, eat, size) {
        super(age, eat);
        this.size = size;
    }
}

class Man extends Human {
    constructor(age, eat, strong) {
        super(age, eat);
        this.strong = strong;
    }
}

const woman = new Woman(22, 10, 3);
const man = new Man(27, 30, 30);

man.eat()
man.eat()
woman.eat()
console.log(woman)
console.log(man)